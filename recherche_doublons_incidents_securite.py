#!/usr/bin/python
# -*- coding: utf8 -*-
from __future__ import unicode_literals
import json
import requests

####### EMPLACEMENT DES DONNEES #######
fichier = 'data/incidents-securite.json'
url='https://ressources.data.sncf.com/api/records/1.0/search/?dataset=incidents-securite&rows=9999&facet=date&facet=esr&facet=type'

class Incident(object):
    def __init__(self,localisation,date,type,esr,commentaires):
        self.localisation = localisation
        self.date = date
        self.type = type
        self.esr = esr
        self.commentaires = commentaires

def json2objets(fichier, url):
    try:
        ####### LECTURE DEPUIS API #######
        req=requests.get(url)
        data = req.json()
        data = data['records']
        ####### LECTURE DEPUIS UN FICHIER JSON #######
        #with open(fichier, 'r') as f:
        #    data = json.load(f)
    except:
        return False
    dico = {}
    for record in data:
        try:
            localisation = record['fields']['localisation']
        except :
            localisation = ''
        date = record['fields']['date']
        esr = record['fields']['esr']
        try:
            type = record['fields']['type']
        except:
            type = ''
        commentaires = record['fields']['commentaires']
        recordid = record['recordid']
        o_incident=Incident(localisation,date,type,esr,commentaires)
        dico[recordid]=o_incident
    return dico

def search_and_print_doublons(dictionnaire):
    dico = {}
    for incident in dictionnaire.values():
        if incident.commentaires != '':
            dico.setdefault(incident.commentaires.upper().replace(' ',''),[]) \
            .append([incident.localisation,
                     incident.date,incident.esr,incident.commentaires])
    for key, data in dico.items():
        if len(data) > 1:
            for i in range(0, len(data)):
                print 'Date : %s\nLieu : %s\nESR : %s\nCommentaires : %s\n' \
                % (dico[key][i][1], dico[key][i][0],
                   dico[key][i][2], dico[key][i][3])
            print '_________________________________________________________'
    return True

dictionnaire = json2objets(fichier, url)
if dictionnaire :
    search_and_print_doublons(dictionnaire)
else :
    print "Erreur lors du chargement des données..."
